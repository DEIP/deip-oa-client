import Vue from 'vue';

import CreateResearchGroupShare from './components/CreateResearchGroupShare';
import CreateResearchGroupQuorum from './components/CreateResearchGroupQuorum';

Vue.component('create-research-group-share', CreateResearchGroupShare);
Vue.component('create-research-group-quorum', CreateResearchGroupQuorum);
