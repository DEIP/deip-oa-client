export { default as AttributesResearchGroupEdit } from './AttributesResearchGroupEdit';
export { default as AttributesResearchGroupRead } from './AttributesResearchGroupRead';
export { default as AttributesResearchGroupSet } from './AttributesResearchGroupSet';
