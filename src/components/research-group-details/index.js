import Vue from 'vue';

// components/group-details
import AddMemberToGroupDialog from './components/AddMemberToGroupDialog';
import QuorumSizeSidebarSection from './components/QuorumSizeSidebarSection';

Vue.component('add-member-to-group-dialog', AddMemberToGroupDialog);
Vue.component('quorum-size-sidebar-section', QuorumSizeSidebarSection);
