export const NAMING_MAP = {
  researchTitle: 'Project name',
  description: 'What is your idea?',
  researchDisciplines: 'Domain',
  location: 'Project location',
  problem: 'What area are you trying to impact?',
  solution: 'How will this solve a current problem?',
  attributes: 'Project readines level',
  funding: 'How much funding are you expecting?',
  eta: 'What is your project estimate?',

  marketResearchAttachment: 'Market research document',
  cvAttachment: 'Resume/CV',
  businessPlanAttachment: 'Business plan',
  budgetAttachment: 'Budget information'
};
