# deip-client-project

> A DEIP client project

Before working with the repo you must be a memeber of @deip npm organization. Ask Yahor Tsaryk for permissions

```

# install dependencies (from root)
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
